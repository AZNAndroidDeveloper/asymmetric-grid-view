package uz.app.lesson39

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.felipecsl.asymmetricgridview.library.model.AsymmetricItem
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridViewAdapter
import uz.app.lesson39.databinding.FragmentIntroBinding

class IntroFragment(var mContext:Context?) :Fragment(R.layout.fragment_intro){
    private lateinit var binding:FragmentIntroBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        val items:MutableList<DemoItem> = ArrayList()
        items.add(DemoItem(1,1,R.drawable.image_bear))
        items.add(DemoItem(1,1,R.drawable.image_bigcat))
        items.add(DemoItem(2,1, R.drawable.image_cat))
        items.add(DemoItem(1,2,R.drawable.image_dog))
        items.add(DemoItem(1,1,  R.drawable.image_elephant))
        items.add(DemoItem(1,1,  R.drawable.image_panda))
        val adapter = AsymmetricListAdapter(requireContext(),items)
        val asymmetricAdapter :AsymmetricGridViewAdapter<*> = AsymmetricGridViewAdapter<AsymmetricItem>(requireContext(),binding.asymmetricGrid,adapter)
        binding.asymmetricGrid.adapter = asymmetricAdapter


    }

//    fun listImage():MutableList<Int> = arrayListOf(
//        R.drawable.image_bear,
//        R.drawable.image_bigcat,
//        R.drawable.image_cat,
//        R.drawable.image_dog,
//        R.drawable.image_elephant,
//        R.drawable.image_panda
//    )

}