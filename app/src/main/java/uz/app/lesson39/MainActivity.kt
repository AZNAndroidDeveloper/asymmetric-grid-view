package uz.app.lesson39

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.felipecsl.asymmetricgridview.library.model.AsymmetricItem
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridViewAdapter
import uz.app.lesson39.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
//        val items:MutableList<DemoItem> = ArrayList()
//        items.add(DemoItem(1,1,R.drawable.image_bear))
//        items.add(DemoItem(1,1,R.drawable.image_bigcat))
//        items.add(DemoItem(2,2, R.drawable.image_cat))
//        items.add(DemoItem(1,2,R.drawable.image_dog))
//        items.add(DemoItem(1,1,  R.drawable.image_elephant))
//        items.add(DemoItem(1,1,  R.drawable.image_panda))
//        val adapter = AsymmetricListAdapter(this,items)
//        val asymmetricAdapter : AsymmetricGridViewAdapter<*> = AsymmetricGridViewAdapter<AsymmetricItem>(this,binding.asymmetricGrid,adapter)
//        binding.asymmetricGrid.adapter = asymmetricAdapter
        supportFragmentManager.beginTransaction().replace(binding.frameLayout.id,IntroFragment(this)).commit()
    }
}