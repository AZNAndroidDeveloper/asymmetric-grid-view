package uz.app.lesson39

import android.os.Parcel
import android.os.Parcelable
import com.felipecsl.asymmetricgridview.library.model.AsymmetricItem

class DemoItem( columnSpan1:Int = 1, rowSpan1:Int = 1,  image:Int  = 0):AsymmetricItem {
    private var columnSpan = 1
    private var rowSpan = 1
    var image= 1
    private set

    init {
        this.columnSpan = columnSpan1
        this.rowSpan = rowSpan1
        this.image = image
    }
    constructor(parcel: Parcel) : this() {
    readFromParcel(parcel)
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
dest!!.writeInt(columnSpan)
dest.writeInt(rowSpan)
dest.writeInt(image)

    }

    override fun getColumnSpan(): Int  = columnSpan

    override fun getRowSpan(): Int= rowSpan

    override fun describeContents(): Int =0
    private fun readFromParcel(parcel:Parcel){
        columnSpan = parcel.readInt()
        rowSpan = parcel.readInt()
        image = parcel.readInt()

    }

    companion object CREATOR : Parcelable.Creator<DemoItem> {
        override fun createFromParcel(parcel: Parcel): DemoItem {
            return DemoItem(parcel)
        }

        override fun newArray(size: Int): Array<DemoItem?> {
            return arrayOfNulls(size)
        }
    }
}