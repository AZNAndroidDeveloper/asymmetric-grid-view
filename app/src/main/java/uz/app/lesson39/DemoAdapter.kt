package uz.app.lesson39

import android.widget.ListAdapter

interface DemoAdapter:ListAdapter {
    fun appendItems(newItems:List<DemoItem>)
    fun setItems(extraItems:List<DemoItem>)
}