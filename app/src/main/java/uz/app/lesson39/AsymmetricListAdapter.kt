package uz.app.lesson39

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView

class AsymmetricListAdapter(context: Context,items:List<DemoItem>):ArrayAdapter<DemoItem>(context,0,items),DemoAdapter
{
    private val layoutInflater:LayoutInflater = LayoutInflater.from(context)


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val item =  getItem(position)!!
        //val isRegularView = getItemViewType(position) == 0
        var view:View = convertView ?: layoutInflater.inflate( R.layout.adapter_item ,parent
             ,false)
        val imageView:ImageView  =
            view.findViewById(R.id.image_view)
        imageView.setImageResource(item.image)


        return view
    }
    override fun appendItems(newItems: List<DemoItem>) {
        addAll(newItems)
        notifyDataSetChanged()
    }

    override fun setItems(extraItems: List<DemoItem>) {
        clear()
        appendItems(extraItems)
    }
//
//    override fun getItemViewType(position: Int): Int {
//        return if (position%2==0) 1 else 0
//
//    }
//
//    override fun getViewTypeCount(): Int {
//        return 2
//    }
}